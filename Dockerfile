FROM oven/bun:1 AS build

WORKDIR /build
COPY . .

RUN apt update && apt install -y curl

RUN curl -sfLS https://install-node.vercel.app | bash -s -- 20 -y

RUN bun install

RUN NITRO_PRESET=bun node node_modules/.bin/nuxi build

RUN rm -rf .output/server/node_modules

RUN cd .output/server && \
    bun install

FROM oven/bun:1-distroless

WORKDIR /app
COPY --from=build /build/.output .

EXPOSE 3000
CMD [ "./server/index.mjs" ]
