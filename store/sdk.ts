import Citadel from "@runcitadel/sdk-next";
import { defineStore } from "pinia";
import useUserStore from "./user";

export interface State {
  citadel: Citadel;
}

const useSdkStore = defineStore("sdk", {
  state: (): State => {
    const state: State = {
      citadel: new Citadel(
        useRuntimeConfig().public.baseURL || window.location.origin,
      ),
    };

    state.citadel.jwt = window.localStorage.getItem("jwt") || "";

    // Redirect back to login on 401
    state.citadel.onAuthFailed = () => {
      // This removes the token everywhere and redirects to login
      try {
        useUserStore().logout();
      } catch {
        // Ignore
      }
      navigateTo("/");
    };

    return state;
  },
  actions: {
    setJwt(newJwt: string) {
      this.citadel.jwt = newJwt;
    },
  },
});

export default useSdkStore;
