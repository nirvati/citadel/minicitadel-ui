import { defineStore } from "pinia";
import useSdkStore from "./sdk";

export interface State {
  loading: boolean;
  unit: "sats" | "btc";
  api: {
    operational: boolean;
    version: string;
  };
  managerApi: {
    operational: boolean;
  };
}

const useSystemStore = defineStore("system", {
  // Initial state
  state: (): State => ({
    loading: true,
    unit: "sats", // sats or btc
    api: {
      operational: false,
      version: "",
    },
    managerApi: {
      operational: false,
    },
  }),
  actions: {
    getUnit() {
      if (window.localStorage.getItem("unit")) {
        this.unit = window.localStorage.getItem("unit") as "sats" | "btc";
      }
    },
    changeUnit(unit: "sats" | "btc") {
      if (unit === "sats" || unit === "btc") {
        window.localStorage.setItem("unit", unit);
        this.unit = unit;
      }
    },
    async getManagerApi() {
      const sdkStore = useSdkStore();
      const api = await sdkStore.citadel.isOnline();
      this.managerApi = {
        operational: api.manager,
      };
    },
  },
});

export default useSystemStore;
