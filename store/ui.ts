import { defineStore } from "pinia";

export interface State {
  isMobileMenuOpen: boolean;
  showBalance: boolean;
}

const useUiStore = defineStore("ui", {
  // Initial state
  state: (): State => ({
    isMobileMenuOpen: false,
    showBalance: true,
  }),
  actions: {
    toggleBalance() {
      this.showBalance = !this.showBalance;
    },
  },
});

export default useUiStore;
